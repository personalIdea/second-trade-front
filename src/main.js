import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import './assets/base.css'
import './assets/common.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
// 0. 如果使用模块化机制编程， 要调用 Vue.use(VueRouter)
Vue.use(VueRouter)
Vue.use(ElementUI)
// 1. 定义（路由）组件。
// 可以从其他文件 import 进来
import Login from './components/login'
import Register from './components/register'
import Index from './components/index'
import AnswerTask from './components/AnswerTask'
import answerQuestion from './components/answerQuestion'
import historyStatic from './components/historyStatic'
import taskManage from './components/taskManage'
import taskStatic from './components/taskStatic'
import taskCreate from './components/taskCreate'
import shopDetail from './components/shopDetail'
import shoppingCar from './components/shoppingCar'
import userDetail from './components/userDetail'
// 2. 定义路由
// 每个路由应该映射一个组件。 其中"component" 可以是
// 通过 Vue.extend() 创建的组件构造器，
// 或者，只是一个组件配置对象。
// 我们晚点再讨论嵌套路由。
const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/index/',
    component: Index,
    children: [
      { path: '', redirect: 'AnswerTask' },
      { path: 'AnswerTask', component: AnswerTask },
      { path: 'taskManage', component: taskManage },
      { path: 'historyStatic', component: historyStatic },
      { path: 'AnswerTask/:title', component: answerQuestion },
      { path: 'taskManage/detail/:id', component: taskStatic },
      { path: 'taskManage/create', component: taskCreate },
      { path: 'shopDetail/:goods_id', component: shopDetail },
      { path: 'shoppingCar', component: shoppingCar },
      { path: 'userDetail', component: userDetail }
    ] }
]

// 3. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const router = new VueRouter({
  routes: routes // （缩写）相当于 routes: routes
})

// 4. 创建和挂载根实例。
// 记得要通过 router 配置参数注入路由，
// 从而让整个应用都有路由功能
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: {
    App
  }
}).$mount('#app')
